<?php
session_start();

	error_reporting(E_ALL);
	ini_set('display_errors','0');

	ini_set('log_errors','1');
	ini_set('error_log', '/home/a4u6th9ent1c/errorlog');

	// include mailing class
	include('mailing.class.php');

	//$site_url = "http://localhost/sites/authentic/";
	$site_url = "http://authenticceylon.com/";
	$site_root = $_SERVER['DOCUMENT_ROOT'].'/';
	//echo $site_root;
	$templates_dir = $site_root .'templates/';
	$assets = $site_root.'assets/';

	$hotels = array(
		'1' => 'Albany - Nuwara Eliya',
		'2' => 'Randoni Villa - Seeduwa',
		'3' => 'Serenity Villa - Wadduwa'
		);

	$roomtypes_1 = array(
		'1' => 'Suit',
		);

	$roomtypes_2 = array(
		'1' => 'The Pool Villa',
		'2' => 'Garden Villa',
		);

	$roomtypes_3 = array(
		'1' => 'Standard Room',
		'2' => 'Full Villa',
		);
