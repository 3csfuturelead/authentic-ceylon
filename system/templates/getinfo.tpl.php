<?php require 'common/header.php'; ?>


<section id="main_content" style="padding-bottom:20px;"><!-- main content -->
	
	<div class="container">
		<div class="row">
		  <!--<div class="col-md-12">
			<small><i></i>Add alerts if form ok... success, else error.</i></small>
			<div class="alert alert-success"><strong><span class="glyphicon glyphicon-send"></span> Success! Message sent. (If form ok!)</strong></div>	  
			<div class="alert alert-danger"><span class="glyphicon glyphicon-alert"></span><strong> Error! Please check the inputs. (If form error!)</strong></div>
		  </div>-->
		  
		  <form role="form" method="post" action="<?php echo $_SERVER['PHP_SELF'].'?info'; ?>" >
			<div class="col-lg-6">
			  <div class="well well-sm" style="width:93%;"><strong><?php echo (isset($validation_errors) && $validation_errors == 1 ? "<i class='glyphicon glyphicon-remove form-control-feedback'></i> <span style='color: red'>There were errors!</span>" : "<i class='glyphicon glyphicon-ok form-control-feedback'></i> Required Field"); ?></strong></div>
			  
			  <div class="form-group">
				<label>Hotel / Villa</label>
				<div class="input-group">
					  <!--  <select class="frm-field required" disabled>
							<option value=""><?php //echo $_SESSION['BOOKINGDATA']['HOTEL']; ?></option>
							<option value='1'>Albany - Nuwara Eliya</option><option value='2'>Randoni Villa - Seeduwa</option><option value='3'>Serenity Villa - Wadduwa</option> 						
							</select> -->
							<input class="form-control" type="text" value="<?php echo $_SESSION['BOOKINGDATA']['HOTEL']; ?>" disabled />
				  <span class="input-group-addon"><i class='glyphicon glyphicon-ok form-control-feedback'></i></span></div>
			  </div>

			  <div class="form-group">
				<label>Room Type</label>
				<div class="input-group">
						<!-- <select class="frm-field required" disabled>
							<option value=""><?php //echo $_SESSION['BOOKINGDATA']['ROOM']; ?></option>
						</select> -->
						<input class="form-control" type="text" value="<?php echo $_SESSION['BOOKINGDATA']['ROOM']; ?>" disabled />
				  <span class="input-group-addon"><i class='glyphicon glyphicon-ok form-control-feedback'></i></span></div>
			  </div>

			  <div class="form-group">
				<label>Check In Date</label>
				<div class="input-group">
				  <input class="form-control" type="text" value="<?php echo $_SESSION['BOOKINGDATA']['CHECKIN']; ?>" disabled />
				  <span class="input-group-addon"><i class='glyphicon glyphicon-ok form-control-feedback'></i></span></div>
			  </div>

			  <div class="form-group">
				<label>Check Out Date</label>
				<div class="input-group">
				  <input class="form-control" type="text" value="<?php echo $_SESSION['BOOKINGDATA']['CHECKOUT']; ?>" disabled />
				  <span class="input-group-addon"><i class='glyphicon glyphicon-ok form-control-feedback'></i></span></div>
			  </div>		  			  			  

			  <div class="form-group">
				<label for="InputName">Your Name</label>
				<div class="input-group">
				  <input type="text" class="form-control" name="customer_name" id="InputName" placeholder="Enter Name" value="<?php echo (isset($_POST['customer_name']) ? $_POST['customer_name'] : ''); ?>" required />
				  <span class="input-group-addon"><?php echo ((isset($error_fields) && in_array('name',$error_fields)) ? "<i class='glyphicon glyphicon-remove form-control-feedback'></i>" : "<i class='glyphicon glyphicon-ok form-control-feedback'></i>"); ?></i></span></div>
			  </div>
			  
			  <div class="form-group">
				<label for="InputEmail">Your Email</label>
				<div class="input-group">
				  <input type="email" class="form-control" id="InputEmail" name="customer_email" placeholder="Enter Email" value="<?php echo (isset($_POST['customer_email']) ? $_POST['customer_email'] : ''); ?>" required />
				  <span class="input-group-addon"><?php echo ((isset($error_fields) && in_array('email',$error_fields)) ? "<i class='glyphicon glyphicon-remove form-control-feedback'></i>" : "<i class='glyphicon glyphicon-ok form-control-feedback'></i>"); ?></span></div>
			  </div>
			  
			  <div class="form-group">
				<label for="InputEmail">Your Contact Number</label>
				<div class="input-group">
				  <input type="text" class="form-control" id="InputEmail" name="customer_phone" placeholder="Enter Contact Number" value="<?php echo (isset($_POST['customer_phone']) ? $_POST['customer_phone'] : ''); ?>" required />
				  <span class="input-group-addon"><?php echo ((isset($error_fields) && in_array('phone',$error_fields)) ? "<i class='glyphicon glyphicon-remove form-control-feedback'></i>" : "<i class='glyphicon glyphicon-ok form-control-feedback'></i>"); ?></span></div>
			  </div>

			  <input type="submit" name="customer_submit" id="submit" value="Submit" class="btn btn-info">
			</div>
		  </form>
		  
		  <hr class="featurette-divider hidden-lg">
		  <div class="col-lg-5 col-md-push-1">
			
			<h2>Rate Sheets</h2>
			<ul>
				<li><a href="<?php echo $site_url; ?>albany-nuwara-eliya/rates">Albany - Nuwara Eliya</a></li>
				<li><a href="<?php echo $site_url; ?>randoni-villa-seeduwa/rates">Randoni Villa - Seeduwa</a></li>
				<li><a href="<?php echo $site_url; ?>serenity-villa-wadduwa/rates">Serenity Villa - Wadduwa</a></li>
			</ul>

			
		  </div>
		</div>
	</div>

</section>

<footer id="footer"><!-- footer -->
	<?php include ('common/footer.php'); ?>
</footer>

</body>
</html>