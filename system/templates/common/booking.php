<div class="details">
	 <div class="container">
		<div class="reservation">
		<form id="bookingfrm" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>" onsubmit="return validate()">
				<ul>
					<li class="span1_of_1">
						<!-- start section_room -->
						<div class="section_room">
						<select id="hotel" name="hotel" class="frm-field required">
							<option value="">Select Hotel / Villa</option>
							<?php

						 		foreach($hotels as $key => $val)
						 		{
						 			echo "<option value='".$key."'>".$val."</option>";
						 		}
						 	?>
						</select>
						</div>	
					</li>
					<li  class="span1_of_1 left">
						<div class="book_date">
								<input class="date" id="datepicker" name="checkin" type="text" value="Check In" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'DD/MM/YY';}" required />
						</div>					
					</li>
					<li  class="span1_of_1 left">
						<div class="book_date">
								<input class="date" id="datepicker1" name="checkout" type="text" value="Check Out" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'DD/MM/YY';}" required />
						</div>		
					</li>
					<li class="span1_of_2 left">
						<!-- start section_room -->
						<div class="section_room">
						<select id="roomtype" name="roomtype" class="frm-field required">
							<option value="">Room Type</option>
						</select>
						</div>					
					</li>
					<li class="span1_of_3">
						
								 <button type="submit" class="btn btn-info" style="margin-top:5px;" id="chkbtn" name="av_check">&nbsp; Enquire &nbsp;</button>
						
					</li>
					<div class="clear"></div>
				</ul>
			</form>
		</div>
		<div class="clear"></div>
		<div class="clearfix"> </div>
	</div>
</div>