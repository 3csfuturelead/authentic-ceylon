<?php require_once('../system/includes/config.php'); ?>
<!DOCTYPE HTML>
<html>
<head>
<?php include ('../system/templates/common/head.php'); ?>
<script defer src="js/jquery.flexslider.js"></script>	
</head>
<body>
<header class="header"><!--- start header -->

	<div class="header-top"><!--- start header top -->
		<?php include ('../system/templates/common/header_top.php'); ?>
	</div> <!--- end header top -->
	
	<div class="header-middle"><!--- navigation menu -->
		<div class="container">
	<div class="row">
		<div class="col-xs-3">
			<div class="logo"> <!--- logo -->
				<a href="<?php echo $site_url; ?>"><img src="images/logo.png" alt="" /></a>
			</div>
		</div>
		
		
			<div class="col-xs-3" class="header-contact">
			<div class="contact"> <!--- logo---->
				 <div class="top_detail1"><p><i class="fa fa-envelope"></i> <a href="mailto:reservations@authenticceylon.com?Subject=Hello%20again" target="_top" style="color:#fff; text-decoration:none;"> &nbsp reservations@authenticceylon.com</a></p></div>
			
			<div style="clear:both;"></div>
			<div class="top_detail1"><p><i class="fa fa-phone-square"></i> &nbsp +94 (0)767 934 234</p></div>
			<div style="clear:both;"></div>
			<div class="social-links1" style="padding:0" >
				<a href="#"><div class="tw" title="Skype us"></div></a>
				<a href="https://www.facebook.com/AuthenticCeylon?ref=hl" target="blank"><div class="fb" title="Like us"></div></a>
				</div>
				</div>
		</div>
		
			<div class="col-xs-6 header_right" >
				<span class="menu"></span>
					<div class="top-menu">
						<ul class="nav_manu">
							<li><a href="<?php echo $site_url; ?>">Home</a></li>
							<li><a class="<?php echo (isset($_GET['pg']) && $_GET['pg'] == 'about' ? 'active' : ''); ?> scroll" href="<?php echo $site_url; ?>about">About Us</a></li>
							<li class="dropdown">
								<a href="<?php echo $site_url; ?>properties" class="dropdown-toggle" data-toggle="dropdown">Properties <i class="<?php echo (isset($_GET['pg']) && $_GET['pg'] == 'properties' ? 'active' : ''); ?> fa fa-angle-down"></i></a>
								<ul class="dropdown-menu">
									<li><a href="<?php echo $site_url; ?>properties/albany-nuwara-eliya">Albany  </a></li>
									<li><a href="<?php echo $site_url; ?>properties/randoni-villa-seeduwa">Randoni </a> </li>
									<li><a href="<?php echo $site_url; ?>properties/serenity-villa-wadduwa">Serenity</a></li>
								</ul>
							</li>
							<li><a href="<?php echo $site_url; ?>team" class="<?php echo (isset($_GET['pg']) && $_GET['pg'] == 'team' ? 'active' : ''); ?>">Travel Advice</a></li>
							<li><a href="<?php echo $site_url; ?>contact-us" class="<?php echo (isset($_GET['pg']) && $_GET['pg'] == 'contact-us' ? 'active' : ''); ?>">Contact us</a></li>
							<div class="clearfix"></div>
						</ul>
					</div>
					<!-- script for menu -->
					<script>
						$( "span.menu" ).click(function() {
						  $( ".top-menu" ).slideToggle( "slow", function() {
							// Animation complete.
						  });
						});
					</script>
					<!-- /script for menu -->
			</div>
	</div>
</div>
	</div>
	
</header><!--- end header -->
<style>
.banner_holder{
    width: 100%;
    height: 350px;
	min-height:200px;
    position: relative;
	border-bottom:8px solid #00a79e;
}

.banner_holderImage{
    height: 100%;
    position:relative;
    background:   url("img/1920/services.jpg")no-repeat;
    background-size: cover;
    background-position: center;
}

</style>

<?php if(isset($_GET['pg']) && in_array($_GET['pg'], array('about','contact-us','services'))): ?>
<section>
   <div class="banner_holder">
    <div class="banner_holderImage"> </div>  
</div>
</section>
<?php endif; ?>