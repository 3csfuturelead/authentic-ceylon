<div class="container">
	<div class="row">
		<div class="col-xs-4">
			<div class="logo"> <!--- logo---->
				<a href="index.php"><img src="images/logo.png" alt="" /></a>
			</div>
		</div>
			<div class="col-xs-8 header_right" >
				<span class="menu"></span>
					<div class="top-menu">
						<ul>
							<li><a class="active scroll" href="/">Home</a></li>
							<li><a  href="about">About Us</a></li>
							<li><a  href="services">Services</a></li>
							<li><a  href="properties">Properties</a></li>
							<li><a  href="team">Team</a></li>
							<li><a  href="contact-us">Contact us</a></li>
							<div class="clearfix"></div>
						</ul>
					</div>
					<!-- script for menu -->
					<script>
						$( "span.menu" ).click(function() {
						  $( ".top-menu" ).slideToggle( "slow", function() {
							// Animation complete.
						  });
						});
					</script>
					<!-- /script for menu -->
			</div>
	</div>
</div>