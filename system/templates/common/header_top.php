<div class="container">
	<div class="row">
		<div class="col-lg-4 col-sm-5 col-md-5">
			 <div class="top_detail"><p><i class="fa fa-envelope"></i> <a href="mailto:reservations@authenticceylon.com?Subject=Hello%20again" target="_top" style="color:#fff; text-decoration:none;"> &nbsp; reservations@authenticceylon.com</a></p></div>
		</div>
		<div class="col-lg-4 col-sm-4 col-md-4">
				<div class="top_detail"><p><i class="fa fa-phone-square"></i> &nbsp; +94 (0)767 934 234</p></div>
		 </div>
		 <div class="col-lg-4 col-sm-3 col-md-3">
				<div class="social-links" style="padding:0" >
				<a href="skype:authentic_ceylon?call"><div class="tw" title="Skype us"></div></a>
				<a href="https://www.facebook.com/AuthenticCeylon?ref=hl" target="_blank"><div class="fb" title="Like us"></div></a>
				</div>
		  </div>
	</div>
</div>