<?php require 'common/header.php'; ?>

<section id="main_content"><!-- main content -->
	<div class="container" style="height:100%;">
			<div class="row">
				<div style="height:100%; ">
				<h1 class="text-center" style="padding-top:100px; color:#00a79e; ">Thank You</h1>
				<h2 class="text-center" style="padding-bottom:100px; color:#69bb54;">Your request has been submited. We will get back to you soon.</h2>
				</div>
			</div>
	</div>
</section>

<footer id="footer"><!-- footer -->
	<?php include ('common/footer.php'); ?>
</footer>

</body>
</html>