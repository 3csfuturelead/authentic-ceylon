<!DOCTYPE HTML>
<html>
<head>
<title>Home</title>
	<base href="<?php echo $site_url .'assets/'; ?>" />
	<link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">
	<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
	<script type="text/javascript">
		var siteurl = "<?php echo $site_url; ?>";
	</script>
</head>

<body>

	<header id="header">
		<div class="top-bar">
			<div class="container">
				<div class="row">
					<div class="col-sm-3">
                        <div class="top-number"><p><i class="fa fa-envelope"></i>  authenticceylon@travel.lk</p></div>
                    </div>
					<div class="col-sm-2" >
                        <div class="top-number"><p><i class="fa fa-phone-square"></i>  +0123 456 70 90</p></div>
                    </div>
				</div>
			</div><!--/.container-->
		</div><!--/.top-bar-->
	<div class="navbar">	
	<div class="col-sm-4">
	  <div class="logo">
		<a href="index.html"><img src="images/logo.png" alt=""/></a>
	  </div>
	</div>
	<div class="col-sm-8 header_right">
	  <span class="menu"></span>
			<div class="top-menu">
				<ul>
					<li><a class="active" href="index.php">Home</a></li>
					<li><a href="about.html">About Us</a></li>
					<li><a href="services.html">Our Services</a></li>
					<li><a href="gallery.html">Our Properties</a></li>
					<li><a href="team.html">The Team</a></li>
					<li><a href="contact.html">Contact</a></li>
					<div class="clearfix"></div>
				</ul>
			 </div>
	</div>
	</div>
		<div class="clearfix"> </div>
		
		<div class="slider">
	  <div class="callbacks_container">
	      <ul class="rslides" id="slider">
		  
	        <li><img src="images/banner.jpg" class="img-responsive" alt=""/>

			</li>
			
	        <li><img src="images/banner1.jpg" class="img-responsive" alt=""/>
	        </li>
			
	        <li><img src="images/banner2.jpg" class="img-responsive" alt=""/>
			 </li>
	      </ul>
	 </div>
</div>
	</header>
	
	<section id="booking">
	
	<div class="details">
		<form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>" >
			     <div class="container">
	    	     <div class="col-xs-10 dropdown-buttons">   
            	  <div class="col-xs-3 dropdown-button">           			
            		<div class="section_room">
						 <select id="hotel" class="frm-field required" name="hotel">
						 	<option value="">Select Hotel/Villa</option>    
						 	<?php

						 		foreach($hotels as $key => $val)
						 		{
						 			echo "<option value='".$key."'>".$val."</option>";
						 		}
						 	?>
						 </select>
					  </div>
					</div>
				     <div class="col-xs-3 dropdown-button">
					  <div class="section_room">
						<input type="text" value="check In Date" id="checkin" name="checkin" />
					  </div>
					 </div>
					 <div class="col-xs-3 dropdown-button">
					  <div class="section_room">
						<input type="text" value="check Out Date" id="checkout" name="checkout" />
					  </div>
					 </div>
					  <div class="col-xs-3 dropdown-button">
					  <div class="section_room">
						 <select id="roomtype" name="roomtype" class="frm-field required">

						 </select>
					  </div>
					 </div>
				   </div> 
				   <div class="col-xs-2 submit_button"> 

				   	     <input type="submit" value="Check Availability" id="chkbtn" name="av_check" />
		</form>		   	     
				   </div>
				   <div class="clearfix"> </div>
				</div>
			   </div>
	</section>
	
	<section id="content">
		<div class="container">
			<div class="row">
				<h2 class="text-center">Authentic Ceylon</h2>
				<p class="text-center">The Sri Lanka Collection represents a hand-picked portfolio of Sri Lanka's most exclusive hotels, villas and safari camps and its two best destination management companies (DMCs) or incoming tour operators.
</p>
			</div>
		</div>
		
	</section>
	
	<footer id="footer">
		<div class="container">
			<div class="row">
			<div class="col-sm-6" class="text-left">
				<p>&copy authenticceylon.lk | <a href="#">Web design by 3CS</a></p>
			</div >
			<div class="col-sm-6" class="stemap">
				<p class="text-right" >Privacy Policy | Site Map</p>
			</div>
				</div>
		</div>
		
	</footer>


	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
	<script src="js/responsiveslides.min.js"></script>
	<script type="text/javascript">


		$( "span.menu" ).click(function() {
			  $( ".top-menu" ).slideToggle( "slow", function() {
			    // Animation complete.
			  });
		});

		$(function () {
		      $("#slider").responsiveSlides({
		      	auto: true,
		      	nav: true,
		      	speed: 500,
		        namespace: "callbacks",
		        pager: true,
		      });
	    });


		$('#hotel').change(function(){
			var hid =$(this).val();
			var dataString = 'id='+ hid;

			$.ajax({
				type: "POST",
				url: siteurl+"ajax/get_rooms.php",
				data: dataString,
				cache: false,
				success: function(html){
					$("#roomtype").html(html);
					console.log(html);
				} 
			});
		})
		

	</script>


</body>
</html>