<html>
<head>
	<title>Inquiry</title>
</head>
<body>
	<h3>New Inquiry</h3>
	<table>
		<tr colspan="2"><th>Customer Info</th></tr>
		<tr>
			<td>Name :</td>
			<td>{name}</td>
		</tr>
		<tr>
			<td>Email :</td>
			<td>{email}</td>
		</tr>
		<tr>
			<td>Phone :</td>
			<td>{phone}</td>
		</tr>
	</table>
	<br /><br />
	<table>
		<tr colspan="2"><th>Booking Info</th></tr>
		<tr>
			<td>Hotel :</td>
			<td>{hotel}</td>
		</tr>
		<tr>
			<td>Room Type :</td>
			<td>{roomtype}</td>
		</tr>
		<tr>
			<td>Check-In Date :</td>
			<td>{checkin}</td>
		</tr>
		<tr>
			<td>Check-Out Date :</td>
			<td>{checkout}</td>
		</tr>
	</table>
</body>
</html>