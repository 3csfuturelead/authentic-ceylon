<?php require 'common/header.php'; ?>

<section class="slider"><!-- slider -->
   <?php include ('common/slider.php'); ?>
</section>

<section id="booking"><!-- booking -->
	<?php include ('common/booking.php'); ?>
</section>

<section id="main_content"><!-- main content -->
	<div class="container">
			<div class="row">
				
				<p class="text-center"  style="padding-left:10px; padding-right:10px; ">We at Authentic Ceylon like to bring you the most authentic Sri Lankan hotels, villas and lodges for your stay in Sri Lanka. These properties are managed and run by our team and we ensure you get the best Sri Lanka has to offer at value for money prices. We are a small team of Sri Lankans passionate about our country and what it has to offer. Talk to us about free travel advice and some hidden gems that you can visit while in Sri Lanka.  
			</p>
			</div>
	</div>
</section>

<footer id="footer"><!-- footer -->
	<?php include ('common/footer.php'); ?>
</footer>

	<script type="text/javascript">
		$('#hotel').change(function(){
			var hid =$(this).val();
			var dataString = 'id='+ hid;

			$.ajax({
				type: "POST",
				url: siteurl+"ajax/get_rooms.php",
				data: dataString,
				cache: false,
				success: function(html){
					$("#roomtype").html(html);
					console.log(html);
				} 
			});
		})

		function validate()
		{
			var error = 0;

			if($('#hotel').val() == "")
			{
				error = 1;
			}
			else if($('#datepicker').val() == "")
			{
				error = 1;
			}
			else if($('#datepicker1').val() == "")
			{
				error = 1;
			}
			else if($('#roomtype').val() == "")
			{
				error = 1;
			}

			if(error == 1)
			{
				alert("Please fill all the details");
				return false;
			}
			
				return true;
			
			
		}


		
	</script>

</body>
</html>