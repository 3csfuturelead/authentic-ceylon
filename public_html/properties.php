<?php require '../system/templates/common/header.php'; ?>


<section id="main_content"><!-- main content -->
	<div class="container">
		<div class="row">
		<div class="col-lg-12 " >
						<div id="demo">

						  <div id="owl-demo" class="owl-carousel">
							<div class="item"><img src="images/randoni/1.jpg" alt="Owl Image"></div>
							<div class="item"><img src="images/randoni/4.jpg" alt="Owl Image"></div>
							<div class="item"><img src="images/randoni/5.jpg" alt="Owl Image"></div>
							<div class="item"><img src="images/randoni/11.jpg" alt="Owl Image"></div>
							<div class="item"><img src="images/albany/1.jpg" alt="Owl Image"></div>
							<div class="item"><img src="images/albany/3.jpg" alt="Owl Image"></div>
							<div class="item"><img src="images/albany/6.jpg" alt="Owl Image"></div>
							<div class="item"><img src="images/albany/9.jpg" alt="Owl Image"></div>
							<div class="item"><img src="images/albany/10.jpg" alt="Owl Image"></div>
							<div class="item"><img src="images/albany/12.jpg" alt="Owl Image"></div>
							<div class="item"><img src="images/serenity/1.jpg" alt="Owl Image"></div>
							<div class="item"><img src="images/serenity/5.jpg" alt="Owl Image"></div>
							<div class="item"><img src="images/serenity/7.jpg" alt="Owl Image"></div>
							<div class="item"><img src="images/serenity/10.jpg" alt="Owl Image"></div>

						  </div>
						</div>
					</div>
					
			<div class="col-lg-4" >
				<div class="grid">
					<a href="<?php echo $site_url; ?>properties/albany-nuwara-eliya"><img  class="img-responsive" src="images/grids-img1.jpg" title="image-name" /></a>
					<h3>Albany</h3>
					<h4>Nuwara Eliya</h4>
					<p>The name Albany is derived from 'Alba' which is the Scottish Gaelic name for Scotland. It is located in the former grounds of the Scottish Club of Little England, the ruins of which are still visible from the bungalow.  Located close to the Race Course and at one of the highest points in the city and boasts...</p>
					<a class="button" href="<?php echo $site_url; ?>properties/albany-nuwara-eliya">View More</a>
				</div>
			</div>
			<div class="col-lg-4">
				<div class="grid">
					<a href="<?php echo $site_url; ?>properties/randoni-villa-seeduwa"><img class="img-responsive" src="images/grids-img2.jpg" title="image-name" /></a>
					<h3>Randoni Villa</h3>
					<h4>Seeduwa</h4>
					<p>Overlooking a wetland and river Randoni Villa is the perfect place to begin and end your holiday. It has 2 individual villas that offer the traveller a relaxing and tranquil atmosphere. Located just 20 minutes from the airport this family owned hotel offers great value for money. Randoni Villa consists...</p>
					<a class="button" href="<?php echo $site_url; ?>properties/randoni-villa-seeduwa">View More</a>
				</div>
			</div>
			<div class="col-lg-4">
				<div class="grid">
					<a href="<?php echo $site_url; ?>properties/serenity-villa-wadduwa"><img class="img-responsive" src="images/grids-img3.jpg" title="image-name" /></a>
					<h3>Serenity Villa</h3>
					<h4>Wadduwa</h4>
					<p>Serenity is a compact four-bedroom villa situated just a short stroll away from the shimmering seas in the coastal village of Wadduwa on the West Coast. Wadduwa is just one hour south of the capital city of Colombo. The journey from Colombo's international airport  will take about two...</p>
					<a class="button" href="<?php echo $site_url; ?>properties/serenity-villa-wadduwa">View More</a>
				</div>
			</div>
			<div class="clear"> </div>
		</div>
		<div class="clear"> </div>
	</div>
</section>

<footer id="footer"><!-- footer -->
	<?php include ('../system/templates/common/footer.php'); ?>
</footer>

</body>
</html>