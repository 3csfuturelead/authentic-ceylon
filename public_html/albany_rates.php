<?php require '../system/templates/common/header.php'; ?>

<section id="albany" ><!-- main content -->
	<div class="container">
		<div class="row">
			<div class="col-lg-12 " >
				<div class="grid1">
					<div class="col-lg-10 " >
					<h1 class="text-left">Albany</h1>
					<h4 class="text-left">Nuwara Eliya</h4>
					
					</div>
					<div class="col-lg-2 " >
					<a href="<?php echo $site_url; ?>reservations/albany-nuwara-eliya">
					 <button  class="btn btn-info">&nbsp Enquire &nbsp</button>
					 </a>
					 <a href="images/albany_rates.jpg" download="Albany Rates.jpg" title="AlbanyRates">
					 <button  class="btn btn-info" style="margin-top:5px;">Download</button></a>
					</div>
					<div class="col-lg-12 " >
					<h6 class="text-left"></h6>
					</div>
					<div class="col-lg-12 " >
					
					
					<img class="img-responsive" src="images/albany_rates_web.jpg" alt="Albany Rates" title="" />
					
					</div>
				</div>
			</div>
			<div class="clear"> </div>
		</div>
		<div class="clear"> </div>
	</div>
</section>

<footer id="footer"><!-- footer -->
	<?php include ('../system/templates/common/footer.php'); ?>
</footer>

</body>
</html>