<?php require '../system/templates/common/header.php'; ?>

<section id="main_content"><!-- main content -->
	<div class="container">
			<div class="row">
				<h2 class="text-center">About us</h2>
				<p class="text-center"  style="padding-left:10px; padding-right:10px;">We are a hotel management team that help locally owned smaller property owners to get their property on the map. Our main focus is Marketing of the property to both local and foreign tour operators and to direct clients who like to book their holidays independently. We also assist in reservations and also the operational aspects of the property. <br> <br>
				If you are an owner of a villa, small hotel, or a eco lodge and would like someone to handle all aspects of the property or just some parts of it like the reservations and marketing of it do drop us an email. 
			</p>
			</div>
	</div>
</section>

<footer id="footer"><!-- footer -->
	<?php include ('../system/templates/common/footer.php'); ?>
</footer>

</body>
</html>