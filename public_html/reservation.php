<?php require_once('../system/includes/config.php'); 

	if($_SERVER['REQUEST_METHOD'] == 'POST')
	{
		// posted
		// get data and sanitize
		$name = trim(strip_tags($_POST['name']));
		$email = trim(strip_tags($_POST['email']));
		$phone = trim(strip_tags($_POST['phone']));
		$hotel = trim(strip_tags($_POST['hotel']));
		$roomtype = trim(strip_tags($_POST['roomtype']));
		$chkindate = trim(strip_tags($_POST['checkin']));
		$chkoutdate = trim(strip_tags($_POST['checkout']));

		// validate data
		$error = 0;
		$errfields = array();
		
		if(empty($name) || is_numeric($name))
		{
			$error = 1;
			$errfields[] = "name";
		}

		if(empty($email))
		{
			$error = 1;
			$errfields[] = "email";
		}
		else if(!filter_var($email, FILTER_SANITIZE_EMAIL))
		{
			$error = 1;
			$errfields[] = "email";
		}

		if(empty($phone))
		{
			$error = 1;
			$errfields[] = "phone";
		}
		else if(!preg_match('/^(\+94)?\s?\d?\d\d\s?\d\d\d\d\d\d\d$/',$phone))
		{
			$error = 1;
			$errfields[] = "phone";
		}

		if(empty($hotel) || !is_numeric($hotel))
		{
			$error = 1;
			$errfields[] = "hotel";
		}

		if(empty($roomtype)|| !is_numeric($roomtype))
		{
			$error = 1;
			$errfields[] = "roomtype";
		}

		if(empty($chkindate))
		{
			$error = 1;
			$errfields[] = "checkin";
		}

		if(empty($chkoutdate))
		{
			$error = 1;
			$errfields[] = "checkout";
		}

		if($error == 0)
		{

			// prepare data
			$hotelname = $hotels[$hotel];

			// room
			switch($hotel)
			{
				case 1:
				// $roomtypes_1
				$roomname = $roomtypes_1[$roomtype];
				break;

				case 2:
				$roomname = $roomtypes_2[$roomtype];
				break;

				case 3:
				$roomname = $roomtypes_3[$roomtype];
				break;
			}


			// no errors
			// good - > prepare and send the email
			// prepare email
			$from 		 = "mailer@authenticceylon.com";
			$subject  	 = "Hotel Booking Inquiry";
			$reply_to	 = $from;

			$email_body = file_get_contents($templates_dir .'emails/request.inc.php');
			$email_body = str_replace('{name}',$name,$email_body);
			$email_body = str_replace('{email}',$email,$email_body);
			$email_body = str_replace('{phone}',$phone,$email_body);
			$email_body = str_replace('{hotel}',$hotelname,$email_body);
			$email_body = str_replace('{roomtype}',$roomname,$email_body);
			$email_body = str_replace('{checkin}',$chkindate,$email_body);
			$email_body = str_replace('{checkout}',$chkoutdate,$email_body);
			
			
			// email data ready -> send
			$receivers = array('reservations@authenticceylon.com');
							
			//$email_body;

			
			foreach($receivers as $receiver)
			{
				try{
					mailing::html_mail($receiver,$subject,$email_body,$from,$reply_to);
				}catch(Exception $e){
					die($e->getMessage());
				}
			}  

			//echo $email_body;

			// success
			header('Location: '.$site_url.'?thankyou');			
		}
	}
?>
<?php require '../system/templates/common/header.php'; ?>


<section id="main_content" style="padding-bottom:20px;"><!-- main content -->
	
	<div class="container">
		<div class="row">
		  <!--<div class="col-md-12">
			<small><i></i>Add alerts if form ok... success, else error.</i></small>
			<div class="alert alert-success"><strong><span class="glyphicon glyphicon-send"></span> Success! Message sent. (If form ok!)</strong></div>	  
			<div class="alert alert-danger"><span class="glyphicon glyphicon-alert"></span><strong> Error! Please check the inputs. (If form error!)</strong></div>
		  </div>-->
		  
		  <form role="form" id="bookingfrm" method="post" action="<?php echo $_SERVER['PHP_SELF'].(isset($_GET['hotel']) ? '?hotel='.$_GET['hotel'] : ''); ?>" onsubmit="return validate()">
			<div class="col-lg-6">
			  <div class="well well-sm" style="width:93%;"><strong><?php echo (isset($error) && $error == 1 ? "<i class='glyphicon glyphicon-remove form-control-feedback'></i> <span style='color: red'>There were errors!</span>" : "<i class='glyphicon glyphicon-ok form-control-feedback'></i> Required Field"); ?></strong></div>
			  
			  <?php if(!isset($_GET['hotel'])): ?>
			  <div class="form-group">
				<label for="hotel">Select Hotel / Villa</label>
				<div class="input-group">
						<select id="hotel" name="hotel" class="frm-field required">
							<option value="">Select Hotel / Villa</option>
							<?php

						 		foreach($hotels as $key => $val)
						 		{
						 			echo "<option value='".$key."'".(isset($_POST['hotel']) && $_POST['hotel'] == $key ? ' selected="selected"' : (isset($_GET['hotel']) && $_GET['hotel'] == $key ? ' selected="selected"' : '')).">".$val."</option>";
						 		}
						 	?>
						</select>
				  <span class="input-group-addon"><?php echo ((isset($errfields) && in_array('hotel',$errfields)) ? "<i class='glyphicon glyphicon-remove form-control-feedback'></i>" : "<i class='glyphicon glyphicon-ok form-control-feedback'></i>"); ?></span></div>
			  </div>
			  <?php endif; ?>
			  
			  <div class="form-group">
				<label for="hotelType">Check In Date</label>
				<div class="input-group">
				  <input class="form-control" id="datepicker" name="checkin" type="text" value="<?php echo (isset($_POST['checkin']) ? $_POST['checkin'] : 'DD/MM/YY'); ?>" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'DD/MM/YY';}" required />
				  <span class="input-group-addon"><?php echo ((isset($errfields) && in_array('checkin',$errfields)) ? "<i class='glyphicon glyphicon-remove form-control-feedback'></i>" : "<i class='glyphicon glyphicon-ok form-control-feedback'></i>"); ?></span></div>
			  </div>
			  
			   <div class="form-group">
				<label for="hotelType">Check out Date</label>
				<div class="input-group">
				  <input class="form-control" id="datepicker1" name="checkout" type="text" value="<?php echo (isset($_POST['checkout']) ? $_POST['checkout'] : 'DD/MM/YY'); ?>" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'DD/MM/YY';}" required />
				  <span class="input-group-addon"><?php echo ((isset($errfields) && in_array('checkout',$errfields)) ? "<i class='glyphicon glyphicon-remove form-control-feedback'></i>" : "<i class='glyphicon glyphicon-ok form-control-feedback'></i>"); ?></span></div>
			  </div>
			  
			  <div class="form-group">
				<label for="hotelType">Select Room Type</label>
				<div class="input-group">
						<select id="roomtype" name="roomtype" class="frm-field required">
							<option value="">Room Type</option>
						</select>
				  <span class="input-group-addon"><?php echo ((isset($errfields) && in_array('roomtype',$errfields)) ? "<i class='glyphicon glyphicon-remove form-control-feedback'></i>" : "<i class='glyphicon glyphicon-ok form-control-feedback'></i>"); ?></span></div>
			  </div>			  

			  <div class="form-group">
				<label for="InputName">Your Name</label>
				<div class="input-group">
				  <input type="text" class="form-control" name="name" id="InputName" placeholder="Enter Name" value="<?php echo (isset($_POST['name']) ? $_POST['name'] : ''); ?>" required />
				  <span class="input-group-addon"><?php echo ((isset($errfields) && in_array('name',$errfields)) ? "<i class='glyphicon glyphicon-remove form-control-feedback'></i>" : "<i class='glyphicon glyphicon-ok form-control-feedback'></i>"); ?></span></div>
			  </div>
			  
			  <div class="form-group">
				<label for="InputEmail">Your Email</label>
				<div class="input-group">
				  <input type="email" class="form-control" id="InputEmail" name="email" placeholder="Enter Email" value="<?php echo (isset($_POST['email']) ? $_POST['email'] : ''); ?>" required  />
				  <span class="input-group-addon"><?php echo ((isset($errfields) && in_array('email',$errfields)) ? "<i class='glyphicon glyphicon-remove form-control-feedback'></i>" : "<i class='glyphicon glyphicon-ok form-control-feedback'></i>"); ?></span></div>
			  </div>
			  
			  <div class="form-group">
				<label for="InputEmail">Your Contact Number</label>
				<div class="input-group">
				  <input type="text" class="form-control" id="InputPhone" name="phone" placeholder="Enter Contact Number" value="<?php echo (isset($_POST['phone']) ? $_POST['phone'] : ''); ?>" required  />
				  <span class="input-group-addon"><?php echo ((isset($errfields) && in_array('phone',$errfields)) ? "<i class='glyphicon glyphicon-remove form-control-feedback'></i>" : "<i class='glyphicon glyphicon-ok form-control-feedback'></i>"); ?></span></div>
			  </div>
			  

			  <?php echo (isset($_GET['hotel']) ? '<input type="hidden" id="hotel" name="hotel" value="'.$_GET['hotel'].'" />' : ''); ?>
			  <input type="submit" name="submit" id="submit" value="Submit" class="btn btn-info">
			</div>
		  </form>
		  
		  <hr class="featurette-divider hidden-lg">
		  <div class="col-lg-5 col-md-push-1">
			
			<h2>Rate Sheets</h2>
			<ul>
				<li><a href="<?php echo $site_url; ?>albany-nuwara-eliya/rates">Albany - Nuwara Eliya </a></li>
				<li><a href="<?php echo $site_url; ?>randoni-villa-seeduwa/rates">Randoni Villa - Seeduwa </a></li>
				<li><a href="<?php echo $site_url; ?>serenity-villa-wadduwa/rates">Serenity Villa - Wadduwa </a></li>
			</ul>

			
		  </div>
		</div>
	</div>

</section>

<footer id="footer"><!-- footer -->
	<?php include ('../system/templates/common/footer.php'); ?>
</footer>
	<script type="text/javascript">

		<?php 
			/*
			if(isset($_GET['hotel']))
			{
				?>

					(function () {

					  //alert("Hello World!");
					  var hotelid = <?php echo (int)$_GET['hotel']; ?>;
					  var dstring = 'id='+hotelid;
						
						  $.ajax({
								type: "POST",
								url: "http://localhost/sites/authentic/ajax/get_rooms.php",
								data: dstring,
								cache: false,
								success: function(html){
									$("#roomtype").html(html);
									console.log(html);
								} 
							});

					})();

				<?php
			}
			*/
		?>

		(function(){

				// get the value of #hotel
				var htid = $('#hotel').val();
				
		
				 if(htid != '')
				 {
				 	
				 	var selectedval = <?php echo (!empty($roomtype) ? $roomtype : '0') ; ?>;
				 	var dstring = 'id='+htid+'&sel='+selectedval;

				 	$.ajax({
						type: "POST",
						url: siteurl+'ajax/get_rooms.php',
						data: dstring,
						cache: false,
						success: function(html){
							$("#roomtype").html(html);
							console.log(html);
						} 
					});
				 }
				  

		})();
		


		$('#hotel').change(function(){
			var hid = $(this).val();
			var dataString = 'id='+ hid;

			$.ajax({
				type: "POST",
				url: siteurl+'ajax/get_rooms.php',
				data: dataString,
				cache: false,
				success: function(html){
					$("#roomtype").html(html);
					console.log(html);
				} 
			});
		})

		/*
		function validate()
		{
			var error = 0;

			if($('#hotel').val() == "")
			{
				error = 1;
			}
			else if($('#datepicker').val() == "")
			{
				error = 1;
			}
			else if($('#datepicker1').val() == "")
			{
				error = 1;
			}
			else if($('#roomtype').val() == "")
			{
				error = 1;
			}

			if(error == 1)
			{
				alert("Please fill all the details");
				return false;
			}
			
				return true;
			
			
		} */


		
	</script>

</body>
</html>