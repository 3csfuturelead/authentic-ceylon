 var oldZoom = $(window).width();
var windowWidth = $(window).width();
check_window_size(windowWidth,1,bsr,bsr_ver);

$(window).resize(function() {
var windowWidthnow = $(window).width();
check_window_size(windowWidthnow,2,bsr,bsr_ver);
}); 

function check_window_size(size,init_var,bsr,bsr_ver)
{
/* Develop for resizing page to avoid grey border!
Page layout 1265px wide. 
On page resize shift layout to keep central, zoom BG-img to fill screen
Zoom content down for smaller screens by 5% to keep content flow!
*/

//change this var for screen width to work with, in this case our site is built at 1265
var wdth = 1265;
 //Change this variable for minimum screen;
var smallest_width=1120;
var varZoom= $(window).width()/wdth;
var s_size = $(window).width();
var scale_smaller;
var center = (s_size-wdth)/2;              
var its_ie=false;


    if(size<=smallest_width)
    {
    $("#old_browser").css("width","50%").css({"height":"40px","left": center+"px"});
                    if(!check_for_object(false,"moved_pages"))
                    {
                                if(center<-110)//margin width!
                                {



                                if(!its_ie)
                                $("#scroller").css("zoom",0.95);
                                $("#footer").css("zoom",0.9).css("left",120+"px");
                                $(".colmask").css("left",-110+"px");
                                if(check_for_object(false,"move_menu_loggedin"))
                                $("#move_menu_loggedin").css("right","110px");
                                if(check_for_object(false,"login_div"))
                                $("#login_div").css("left","-80px");
                                return;
                                }
                                $("#move_menu_loggedin").css("left","-"+center+"px");
                                $("#scroll").css("zoom","normal");
                                $(".colmask").css("left",center+"px");  
                    }
                    else
                    {
                    /*Only pages that you do not want to move the colmask for!*/
                            $("#scroller").css("zoom",0.90);//.css("left","-50px");;
                            $("#footer").css("zoom","normal");

                    }
    }
    else
    {
    if(size>wdth)
    $("#background").css("zoom",varZoom);

    $("#scroller").css("zoom","normal");
    $("#footer").css({"zoom":"normal","left":0});

                        if(!check_for_object(false,"moved_pages"))
                        {

                                        $(".colmask").css("left",center+"px");          
                                        $(".colmask").css("zoom","normal");

                                        var movelog = -center;
                                        if(check_for_object(false,"move_menu_loggedin"))
                                        $("#move_menu_loggedin").css("right",movelog +"px");
                                        if(check_for_object(false,"login_div"))
                                        $("#login_div").css("left","80px");


                        }
                        else
                        {
                        $(".colmask").css("zoom","normal");
                        }
                        }
}