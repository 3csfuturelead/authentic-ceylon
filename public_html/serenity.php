<?php require '../system/templates/common/header.php'; ?>

<section id="albany" ><!-- main content -->
	<div class="container">
		<div class="row">
			<div class="col-lg-12 " >
				<div class="grid1">
					<div class="col-lg-10 " >
					<h1 class="text-left">Serenity Villa</h1>
					<h4 class="text-left">Wadduwa</h4>
					
					</div>
					<div class="col-lg-2 " >
					<a href="<?php echo $site_url; ?>reservations/serenity-villa-wadduwa">
					 <button  class="btn btn-info" style="margin-top:5px;" >Enquire</button></a>
					 <a href="<?php echo $site_url; ?>serenity-villa-wadduwa/rates">
					 <button  class="btn btn-info" style="margin-top:5px;">&nbsp; Rates &nbsp;</button></a>
					</div>
					<div class="col-lg-12 " >
					<h6 class="text-left"></h6>
					</div>
					
					<div class="col-lg-12 " >
						<div id="demo">

						  <div id="owl-demo" class="owl-carousel">
							<div class="item"><img src="images/serenity/1.jpg" alt="Owl Image"></div>
							<div class="item"><img src="images/serenity/2.jpg" alt="Owl Image"></div>
							<div class="item"><img src="images/serenity/3.jpg" alt="Owl Image"></div>
							<div class="item"><img src="images/serenity/4.jpg" alt="Owl Image"></div>
							<div class="item"><img src="images/serenity/5.jpg" alt="Owl Image"></div>
							<div class="item"><img src="images/serenity/6.jpg" alt="Owl Image"></div>
							<div class="item"><img src="images/serenity/7.jpg" alt="Owl Image"></div>
							<div class="item"><img src="images/serenity/8.jpg" alt="Owl Image"></div>
							<div class="item"><img src="images/serenity/9.jpg" alt="Owl Image"></div>
							<div class="item"><img src="images/serenity/10.jpg" alt="Owl Image"></div>
						  </div>
						</div>
					</div>
					
					<div class="col-lg-12 " >
					
					<p class="text-left">Serenity is a compact four-bedroom villa situated just a short stroll away from the shimmering seas in the coastal village of Wadduwa on the West Coast. Wadduwa is just one hour south of the capital city of Colombo. The journey from Colombo's international airport will take about two hours.<br> 
					Fully equipped and self-contained, the villa offers comfortable accommodation, a swimming pool and much privacy. Rise early and take a walk to the beach and watch as the local fisherman bring in their day’s catch; spend your day taking in the sun by the poolside and by dusk go for a stroll and catch the sunset. 
					</p>
					<h2 class="text-left">Accommodation</h2>
					
					<p class="text-left">Of the four ensuite bedrooms, two are more spacious and are furnished with king-size beds. The other two bedrooms are smaller in size. All rooms have a/c and overhead fans. There is also a baby’s cot fitted with mosquito netting. Serenity can accommodate up to eight guests. And is sold by the villa and by the room. When you book the two Master bedrooms you get the full villa for exclusive use. 
					<br>  
					Serenity has a spacious lounge area opening out to the swimming pool. Furnished with comfortable sofas, hours can be spent here with a good book. There is a dining room with an eight seater dining table, you can sit out in the garden and have your meals as well. fully equipped pantry with a fridge, cooker, crockery and cutlery; satellite TV and wifi internet. A in-house cook is provided who will prepare simple meals.  
					</p>
						
					</div>
					<div class="clear"></div>
					
			
				</div>
			</div>
			<div class="clear"> </div>
		</div>
		<div class="clear"> </div>
	</div>
</section>

<footer id="footer"><!-- footer -->
	<?php include ('../system/templates/common/footer.php'); ?>
</footer>

</body>
</html>