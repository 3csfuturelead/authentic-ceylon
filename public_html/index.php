<?php
require('../system/includes/config.php');

	//$show = 'index.tpl.php'

	if(isset($_GET['info']))
	{
		$show = 'getinfo.tpl.php';
	}
	else if(isset($_GET['thankyou']))
	{
		$show = 'response.tpl.php';
	}
	else
	{
		$show = 'index.tpl.php';
	}
	
	if(isset($_POST['av_check']))
	{
		// posted
		$_SESSION['BOOKINGDATA']['HOTEL'] = $hotels[$_POST['hotel']];
		
		

		switch($_POST['hotel'])
		{
			case 1:
			// $roomtypes_1
			$room = $roomtypes_1[$_POST['roomtype']];
			break;

			case 2:
			$room = $roomtypes_2[$_POST['roomtype']];
			break;

			case 3:
			$room = $roomtypes_3[$_POST['roomtype']];
			break;
		}

		$_SESSION['BOOKINGDATA']['ROOM'] = $room;

		$_SESSION['BOOKINGDATA']['CHECKIN'] = $_POST['checkin'];

		$_SESSION['BOOKINGDATA']['CHECKOUT'] = $_POST['checkout'];

		/*
		echo "<pre>";
		print_r($_SESSION['BOOKINGDATA']);
		echo "</pre>"; */

		// lets show the client information page
		header('Location: '.$site_url.'?info');
	}

	if(isset($_POST['customer_submit']))
	{

		$validation_errors = 0;
		$error_fields = array();
		//$error_msgs = array();
		/*
		echo "<pre>";
		print_r($_POST);
		echo "</pre>"; */



		if(!isset($_POST['customer_name']) || empty($_POST['customer_name']))
		{
			$validation_errors = 1;
			$error_fields[] = "name";
			//$error_msgs['name'] = "Please e"
			
		}

		if(!isset($_POST['customer_email']) || empty($_POST['customer_email']))
		{
			$validation_errors = 1;
			$error_fields[] = "email";
			//$error_msgs['']
		}
		else if(!filter_var($_POST['customer_email'], FILTER_VALIDATE_EMAIL))
		{
			$validation_errors = 1;
			$error_fields[] = "email";
		}

		if(!isset($_POST['customer_phone']) || empty($_POST['customer_phone']))
		{
			$validation_errors = 1;
			$error_fields[] = "phone";
		}
		else if(!preg_match('/^(\+94)?\s?\d?\d\d\s?\d\d\d\d\d\d\d$/',$_POST['customer_phone']))
		{
			$validation_errors = 1;
			$error_fields[] = "phone";
		}

		if($validation_errors == 0)
		{
			// good - > prepare and send the email
			// prepare email
			$from 		 = "mailer@authenticceylon.com";
			$subject  	 = "Hotel Booking Inquiry";
			$reply_to	 = $from;

			$email_body = file_get_contents($templates_dir .'emails/request.inc.php');
			$email_body = str_replace('{name}',$_POST['customer_name'],$email_body);
			$email_body = str_replace('{email}',$_POST['customer_email'],$email_body);
			$email_body = str_replace('{phone}',$_POST['customer_phone'],$email_body);
			$email_body = str_replace('{hotel}',$_SESSION['BOOKINGDATA']['HOTEL'],$email_body);
			$email_body = str_replace('{roomtype}',$_SESSION['BOOKINGDATA']['ROOM'],$email_body);
			$email_body = str_replace('{checkin}',$_SESSION['BOOKINGDATA']['CHECKIN'],$email_body);
			$email_body = str_replace('{checkout}',$_SESSION['BOOKINGDATA']['CHECKOUT'],$email_body);
			
			// email data ready -> send
			$reveivers = array('reservations@authenticceylon.com');
							
			//$email_body;

			
			foreach($reveivers as $receiver)
			{
				try{
					mailing::html_mail($receiver,$subject,$email_body,$from,$reply_to);
				}catch(Exception $e){
					die($e->getMessage());
				}
			} 

			//echo $email_body;

			// success
			header('Location: '.$site_url.'?thankyou');
			
		}
	}


	require($templates_dir.$show);