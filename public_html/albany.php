<?php require '../system/templates/common/header.php'; ?>

<section id="albany" ><!-- main content -->
	<div class="container">
		<div class="row">
			<div class="col-lg-12 " >
				<div class="grid1">
					<div class="col-lg-10 " >
					<h1 class="text-left">Albany</h1>
					<h4 class="text-left">Nuwara Eliya</h4>
					
					</div>
					<div class="col-lg-2 " >
					<a href="<?php echo $site_url; ?>reservations/albany-nuwara-eliya">
					 <button  class="btn btn-info" style="margin-top:5px;" >Enquire</button></a>
					 <a href="<?php echo $site_url; ?>albany-nuwara-eliya/rates">
					 <button  class="btn btn-info" style="margin-top:5px;"> &nbsp Rates &nbsp </button></a>
					</div>
					<div class="col-lg-12 " >
					<h6 class="text-left"></h6>
					</div>
					
					<div class="col-lg-12 " >
						<div id="demo">

						  <div id="owl-demo" class="owl-carousel">
							<div class="item"><img src="images/albany/1.jpg" alt="Owl Image"></div>
							<div class="item"><img src="images/albany/2.jpg" alt="Owl Image"></div>
							<div class="item"><img src="images/albany/3.jpg" alt="Owl Image"></div>
							<div class="item"><img src="images/albany/4.jpg" alt="Owl Image"></div>
							<div class="item"><img src="images/albany/5.jpg" alt="Owl Image"></div>
							<div class="item"><img src="images/albany/6.jpg" alt="Owl Image"></div>
							<div class="item"><img src="images/albany/7.jpg" alt="Owl Image"></div>
							<div class="item"><img src="images/albany/8.jpg" alt="Owl Image"></div>
							<div class="item"><img src="images/albany/9.jpg" alt="Owl Image"></div>
							<div class="item"><img src="images/albany/10.jpg" alt="Owl Image"></div>
							<div class="item"><img src="images/albany/11.jpg" alt="Owl Image"></div>
							<div class="item"><img src="images/albany/12.jpg" alt="Owl Image"></div>
							<div class="item"><img src="images/albany/13.jpg" alt="Owl Image"></div>
							<div class="item"><img src="images/albany/14.jpg" alt="Owl Image"></div>
						  </div>
						</div>
					</div>
					
					
					<div class="col-lg-12 " >
					
						<p class="text-left">The name Albany is derived from 'Alba' which is the Scottish Gaelic name for Scotland. It is located 
						in the former grounds of the Scottish Club of Little England, the ruins of which are still visible from the  bungalow.  Located  close  to  the  Race  Course  and  at  one  of  the highest  points  in  the  city and  boasts of wonderful views. <br><br>
						The bungalow was carefully constructed and expanded upon the existing house from the colonial period preserving colonial architectural style features. The interior was designed and furnished with a  perfect  blend  of  the  old  and  new  presenting  cosy  old  English  charm  with  modern  standards  of luxury  and  comfort.  Albany  is  set  amidst  a  beautifully  landscaped  garden  blending  in  with  the neighbouring vegetable plantations. 
						</p>
					</div>
					
					<div class="col-lg-12 " >
						<h2 class="text-left">Accommodation  </h2>
						<p class="text-left">All of our accommodation options offer every modern luxury and comfort while preserving the class and character from the days of the British rule.  Each suite or room comes with garden access or a balcony for you to enjoy the splendid views of the city and the surrounding landscape. The modern bathroom comprises of rain shower with body jets and our range of amenities 
						</p>
							
						<h2 class="text-left">Suites  </h2>
						
						<p class="text-left">You may choose the spacious single floor suite or a suite with a mezzanine floor which separates the sofa bed, coffee table and television into a deck above the bed and has the option of connecting with the adjoining deluxe room. The modern bathroom comprises of his and her vanity, rain shower with body jets and our range of amenities. 
						</p>
						
						<div class="col-lg-6 col-sm-6 text-left">
								
									<ul>
										<li> King size bed with luxury bedding </li>
										<li>Wing chairs and coffee table</li>
										<li>Sofa bed</li>
										<li>Television</li>
										<li>Free Wifi </li>
										<li>Telephone</li>
										<li>Wardrobe </li>
									</ul>
										
						</div>
						
						<div class="col-lg-6 col-sm-6 text-left">
								
									<ul>
										<li>Tea and coffee making facilities  </li>
										<li>Hair Dryer</li>
										<li>Bath robes and slippers</li>
										<li>Room heater (On Request)</li>
										<li>Iron and Ironing board (On Request) </li>
										<li>Baby Cots (On Request) </li>
										
									</ul>
						</div>	
							<div class="clear"></div>
					</div>
					
					<div class="clear"></div>
					
					<div class="col-lg-12 col-sm-12 " >
					<p></p>
						<h2 class="text-left">Nuwara Eliya - Sri Lanka</h2>
						
						<p class="text-left">Nawara Eliya is a city in the hill country of Sri Lanka founded by Sir Samuel Baker in 1846. The meaning  of  the  Singhalese  words  "Nuwara  Eliya"  is  "City  on  the  Plane"  or  "City  of  Light".  It  was named  "Little  England"  during  the  British  Period  and  was  used  as  a  hill  country  retreat  where pastimes such as hunting, horse riding, polo, golf and cricket were popular among visitors. All of the buildings retain features from the colonial period and are furnished in the colonial style and maintain old English-style lawns and gardens. Being located at an altitude of 6,128 feet gives Nuwara Eliya a pleasant  subtropical  highland  climate  with  a  mean  annual  temperature  of  16  °C  (61  °F).  Nuwara Eliya is overlooked by Pidurutalagala, the tallest mountain in Sri Lanka and is surrounded by fruit and vegetable plantations along with the odd water fall or stream. Being the most important location for  Tea  Plantation  in  Sri  Lanka  further  enriches  the  picturesque  mountain  landscapes  on  offer. Boasting  of  attractions  such  as  the  Gregory’s  Lake,  The  Golf  Club,  Race  Course,  Victoria  Park, Galway Forest and the Horton Plains National Park, World's End and Haggala Botanical Gardens, Nuwara Eliya is a true hill country resort.  
						</p>
						
					</div>
					
					<div class="col-lg-12 " >
						<div id="demo">

						  <div id="owl1-demo1" class="owl-carousel">
							<div class="item"><img src="images/nuwara_eliya/1.jpg" alt="Owl Image"></div>
							<div class="item"><img src="images/nuwara_eliya/2.jpg" alt="Owl Image"></div>
							<div class="item"><img src="images/nuwara_eliya/3.jpg" alt="Owl Image"></div>
							<div class="item"><img src="images/nuwara_eliya/4.jpg" alt="Owl Image"></div>
							<div class="item"><img src="images/nuwara_eliya/5.jpg" alt="Owl Image"></div>
							<div class="item"><img src="images/nuwara_eliya/6.jpg" alt="Owl Image"></div>
						  </div>
						</div>
					</div>
					
					<!--<div class="col-lg-12 " >
					<p></p>
						<h2 class="text-left">Deluxe Room</h2>
						
						<p class="text-left">Our Deluxe room offers cosy comfort with its old English charm. The mezzanine floor furnished with a sofa bed, coffee table and television will keep you warm and entertained. The deluxe room has a balcony for you to enjoy the splendid views of the city and the surrounding landscape. The deluxe room  also  comes  with  the  option  of  connecting  with  the  adjoining  suite.  The  modern  bathroom comprises of a rain shower with body jets and our range of amenities. 
						</p>
						
						<div class="col-lg-6 col-sm-6 text-left">
								
									<ul>
										<li>Queen size bed with luxury bedding </li>
										<li> Sofa bed </li>
										<li>Television</li>
										<li>Free Wifi </li>
										<li>Telephone</li>
										<li>Wardrobe </li>
									</ul>
										
						</div>
						
						<div class="col-lg-6 col-sm-6 text-left">
								
									<ul>
										<li>Tea and coffee making facilities  </li>
										<li>Hair Dryer</li>
										<li>Bath robes and slippers</li>
										<li>Room heater (On Request)</li>
										<li>Iron and Ironing board (On Request) </li>
										<li>Baby Cots (On Request) </li>
									</ul>
								
						</div>
						
						<div class="col-lg-12 " style="margin-left: -15px;" >
						<p></p>
							<div class="col-lg-3 col-sm-3 col-md-3 col-xs-3">
								<div class="css3gallery">
									<img class="img-responsive" src="images/pic9.jpg" alt="fotograph" title="" />
								</div>
							<div class="clear"></div>
							</div>
							<div class="col-lg-3 col-sm-3 col-md-3 col-xs-3">
								<div class="css3gallery">
									<img class="img-responsive" src="images/pic10.jpg"  alt="fotograph" title="" />
								</div>
							<div class="clear"></div>
							</div>
							<div class="col-lg-3 col-sm-3 col-md-3 col-xs-3">
								<div class="css3gallery">
									<img class="img-responsive" src="images/pic11.jpg"  alt="fotograph" title="" />
								</div>
							<div class="clear"></div>
							</div>
							<div class="col-lg-3 col-sm-3 col-md-3 col-xs-3">
								<div class= "css3gallery">
									<img class="img-responsive" src="images/pic12.jpg"  alt="fotograph" title="" />
								</div>
							<div class="clear"></div>
							</div>
							<div class="clear"></div>
						</div>
						</div>-->
				</div>
			</div>
			<div class="clear"> </div>
		</div>
		<div class="clear"> </div>
	</div>
</section>

<footer id="footer"><!-- footer -->
	<?php include ('../system/templates/common/footer.php'); ?>
</footer>

</body>
</html>