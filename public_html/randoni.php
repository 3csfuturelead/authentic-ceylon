<?php require '../system/templates/common/header.php'; ?>

<section id="albany" ><!-- main content -->
	<div class="container">
		<div class="row">
			<div class="col-lg-12 " >
				<div class="grid1">
					<div class="col-lg-10 " >
					<h1 class="text-left">Randoni Villa</h1>
					<h4 class="text-left">Seeduwa</h4>
					
					</div>
					<div class="col-lg-2 " >
					<a href="<?php echo $site_url; ?>reservations/randoni-villa-seeduwa">
					 <button  class="btn btn-info" style="margin-top:5px;" >Enquire</button></a>
					 <a href="<?php echo $site_url; ?>randoni-villa-seeduwa/rates">
					 <button  class="btn btn-info" style="margin-top:5px;"> &nbsp Rates &nbsp</button></a>
					</div>
					<div class="col-lg-12 " >
					<h6 class="text-left"></h6>
					</div>
					
					<div class="col-lg-12 " >
						<div id="demo">

						  <div id="owl-demo" class="owl-carousel">
							<div class="item"><img src="images/randoni/1.jpg" alt="Owl Image"></div>
							<div class="item"><img src="images/randoni/2.jpg" alt="Owl Image"></div>
							<div class="item"><img src="images/randoni/3.jpg" alt="Owl Image"></div>
							<div class="item"><img src="images/randoni/4.jpg" alt="Owl Image"></div>
							<div class="item"><img src="images/randoni/5.jpg" alt="Owl Image"></div>
							<div class="item"><img src="images/randoni/6.jpg" alt="Owl Image"></div>
							<div class="item"><img src="images/randoni/7.jpg" alt="Owl Image"></div>
							<div class="item"><img src="images/randoni/8.jpg" alt="Owl Image"></div>
							<div class="item"><img src="images/randoni/9.jpg" alt="Owl Image"></div>
							<div class="item"><img src="images/randoni/10.jpg" alt="Owl Image"></div>
							<div class="item"><img src="images/randoni/11.jpg" alt="Owl Image"></div>
							<div class="item"><img src="images/randoni/12.jpg" alt="Owl Image"></div>
						  </div>
						</div>
					</div>
					
					<div class="col-lg-12 " >
					
					<p class="text-left">Overlooking a wetland and river Randoni Villa is the perfect place to begin and end your holiday. It has 2 individual villas that offer the traveller a relaxing and tranquil atmosphere. Located just 20 minutes from the airport this family owned hotel offers great value for money.  
					</p>
					<h2 class="text-left">Introduction</h2>
					
					<p class="text-left">Randoni Villa consists of just two chalets, a swimming pool and a pavilion to have meals which are immersed in lush tropical gardens. The chalets and pavilions have verandahs and courtyards under the shade of tall tropical foliage. Interiors are colourful and offer comfort and space. Enter through the unassuming reception into this garden sanctuary of peace and calm. Relax by the poolside edged by river; or learn to cook a traditional Sri Lankan meal in clay pots over a wood-fired hearth. Both chalets can be booked exclusively by one party or individually and can accommodate up to four guests. Children 8 years and above are encouraged due to safety reasons. <br>  
					We offer home cooked Sri Lankan meals along with a selection of western dishes. Dinning is available during any time of the day.  
					</p>

					</div>
					<div class="clear"></div>
					<div class="col-lg-12 " >
						<p></p>
						<p></p>
						<h2 class="text-left">Accommodation  </h2>
						<p class="text-left">There are two chalets – The Pool Villa  and Garden Villa situated near each other but offering each other privacy. Both chalets are self-contained and consist of a bedroom an outside dining area and have a/c, overhead fans, and mosquito nets. The bedrooms are comfortably furnished with king-size beds. A writing table is available for anyone who wishes to while away some time. There is an ensuite bathroom with a w/c and basin and a large shower area and h/w shower.
						<br>
						Facilities: Swimming pool, Internet, in room dinning, Eco friendly, Bird Watching 
						</p>
							
						<h2 class="text-left">Excursions</h2>
						
						<p class="text-left">The river is excellent for bird watching and the wetland surrounding it offers refuge for many resident and migrant birds. Butterflies are plenty in this garden as well with its riverine forest. Spend the day relaxing by the pool or drive up to Negombo for a tour of the fishing village and old Dutch fort. Take the highway to Colombo and explore the capital city with a Colombo City walk and lunch at one of the signature restaurants in Colombo.  
						</p>
						
						
					</div>	
			
				</div>
			</div>
			<div class="clear"> </div>
		</div>
		<div class="clear"> </div>
	</div>
</section>

<footer id="footer"><!-- footer -->
	<?php include ('../system/templates/common/footer.php'); ?>
</footer>

</body>
</html>