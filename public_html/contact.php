<?php require '../system/templates/common/header.php'; ?>

<section id="main_content"><!-- main content -->
	<div class="container">
		<div class="row-fluid">
			<div class="col-lg-7">
				<div class="row">
					<h3 class="brea_heading text-left" >Contact Us</h3>
					<form method="post" role="form" class="contact_us" onsubmit="return validate()">
						<?php
							if(isset($error) && $error == 1)
							{
								?>
								<div style="background: red; color: white; list-style: none;">
									<p>There were errors in your submission. Please try again</p>
									<ul>
										<?php
											foreach($errmsgs as $val)
											{
												?>
												<li><?php echo $val; ?></li>
												<?php
											}
										?>
									</ul>
								</div>
								<?php
							}
						?>
						<div class="form-group">
						  <label for="name">Name</label>
						  <input type="text" class="form-control" name="name" id="name" placeholder="Enter Name" required />
					   </div>
					   <div class="form-group">
						  <label for="email">Email</label>
						  <input type="email" class="form-control" name="email" id="email" placeholder="Enter Email" required />
					   </div>
					   <!--<div class="form-group">
						  <label for="subject">Subject</label>
						  <input type="text" class="form-control" name="subject" id="subject" placeholder="Enter subject" required />
					   </div> -->
						<div class="form-group">
						  <label for="message">Enquiry</label>
						  <textarea name="message" class="form-control" id="message" placeholder="Enter message" required></textarea>
						</div>
						<div class="form_button">
					   <button type="submit" class="btn btn-info">SEND</button>
					   </div>
					</form>		
				</div>
			</div>
			<div class="col-lg-5">
				<div class="row">
					<h3 class="brea_heading" >We Are Here</h3>
						<div id="map-container2" class="col-md-12"></div>
						<br/>
					<h3 class="brea_heading2" >Address</h3>
		
					<p> 723/128, Lake Terrace,<br>Athurugiriya,<br/>SRI LANKA</p>
					<p><i class="fa fa-phone-square"></i> &nbsp;  +94 (0)767 934 234 <br>
						<i class="fa fa-envelope"></i> <a href="mailto:reservations@authenticceylon.com?Subject=Hello%20again" target="_top" style="color:#333; text-decoration:none;"> &nbsp reservations@authenticceylon.com</a>
					</p>
					<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->

					<!-- Include all compiled plugins (below), or include individual files as needed -->

					<script src="http://maps.google.com/maps/api/js?sensor=false"></script>
					<script>	
				 
						function init_map() {
							var var_location = new google.maps.LatLng(6.878682, 79.990847);
							 
									var var_mapoptions = {
									  center: var_location,
									  zoom: 14
									};
							 
							var var_marker = new google.maps.Marker({
							position: var_location,
							map: var_map,
							title:"Venice"});
							 
									var var_map = new google.maps.Map(document.getElementById("map-container2"),
										var_mapoptions);
							 
							var_marker.setMap(var_map);	
						 
							}
						 
							 google.maps.event.addDomListener(window, 'load', init_map);
				 
					</script>
					<script type="text/javascript">
						function validate()
						{
							if($('#name').val() == '')
							{
								alert('Please enter your name');
								return false;
							}
							else if($('#email').val() == '')
							{
								alert('Please enter your email address');
								return false;
							}
							else if($('#subject').val() == '')
							{
								alert('Please enter your message subject');
								return false;
							}
							else if($('#message').val() == '')
							{
								alert('Please enter your message');
								return false;
							}

							//alert('all good');
							return true;
						}
					</script>			
				</div>
			</div>
		</div>
	</div>
</section>

<footer id="footer"><!-- footer -->
	<?php include ('../system/templates/common/footer.php'); ?>
</footer>

</body>
</html>